Vue.component('task-list', {

    template: `
            <div>
                <task v-for="task in tasks"> {{task.description}}</task>
            </div>`,

    data() {
        return {
            tasks: [
                { description: "Go to the store", completed: true },
                { description: "Go to schoo", completed: true },
                { description: "have lunch", completed: false },
                { description: "Go to the store again", completed: true },
                { description: "Make dinner", completed: true },
                { description: "Sleep", completed: false }

            ]

        }
    }
})

Vue.component('task', {

    template: '<li><slot></slot></li>'
})

new Vue({

    el: '#root'
})