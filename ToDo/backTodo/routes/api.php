<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Auth\AuthenticateController@create');
Route::post('login', 'Auth\AuthenticateController@authenticate');
Route::post('recover', 'Auth\AuthenticateController@recover');
Route::group(['middleware' => ['jwt.auth']], function () {
    Route::get('logout', 'Auth\AuthenticateController@logout');
    Route::get('test', function () {
        return response()->json(['foo' => 'bar']);

    });
    Route::put('tasks/{id}','TaskController@update');
    Route::get('tasks', 'Auth\AuthenticateController@userTasks');
    Route::delete('tasks/{id}', 'TaskController@destroy');
    Route::post('tasks', 'TaskController@create');
});
