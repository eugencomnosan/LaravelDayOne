import axios from 'axios'
import { APP_URL } from '../constants/index'

export default {
  register(name, email, password) {
    return axios.post(APP_URL + 'api/register',
      {
        name: name,
        email: email,
        password: password
      })
  },
  login(email, password) {
    return axios.post(APP_URL + 'api/login',
      {
        email: email,
        password: password
      }).then(({ data }) => {
        axios.defaults.headers['Authorization']=`Bearer ${data.token}`
        localStorage.setItem('token', data.token);
      //  axios.defaults['headers']['Authorization'] = `Bearer ${data.token}`,
        console.log(axios.defaults.headers);
      });
  }
}
