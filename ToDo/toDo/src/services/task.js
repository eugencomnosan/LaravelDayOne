import axios from 'axios'
import { APP_URL } from '../constants/index'

export default {
    create(content, priority, completed) {
        return axios.post(APP_URL + 'api/tasks',
            {
                content: content,
                priority: false,
                completed: false
            })
    },
    get() {
        console.log(axios.defaults.headers);
        return axios.get(APP_URL + 'api/tasks');
    },
    update(taskId, content, completed, priority) {
        return axios.put(APP_URL + `api/tasks/${taskId}`,
            {
                content: content,
                priority: priority,
                completed: completed
            }
        )
    },
    delete(taskId){
        return axios.delete(APP_URL+ `api/tasks/${taskId}`)
    }




}