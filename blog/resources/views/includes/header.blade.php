

<div class="navbar">
    <div class="navbar-inner">
        <a id="logo">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>
        <ul class="nav">
            <li><a href="/">Home</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/projects">Projects</a></li>
            <li><a href="/contact">Contact</a></li>
        </ul>
    </div>
</div>