<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth/login');
})->middleware('guest');

Route::get('/logout','Auth\LoginController@logout' )->middleware('auth');



Route::get('/home', 'HomeController@show')->middleware('auth');

Route::get('/alertChild', function(){
    return view('alertChild');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
