import { LabPregled, NivoSecera } from "./labPregled";

class ZdravstvenaUstanova {
    constructor(doktori = [], pacijenti = [], pregledi = []) {
        this.studenti = studenti;
        this.predmeti = predmeti;
        this.pregledi = pregledi;
    }

    dodajDoktora(doktor) {
        this.doktori.push(doktor);
    }

    dodajPacijenta(pacijent) {
        this.pacijenti.push(pacijent);
    }

    dodajPregled(doktor, pacijent, datum) {
        const idd = SluzbaUtil.indexOfDoktor(this.doktori, doktor.ime, doktor.prezime);
        if (idx !== -1) {
            this.pregledi[idx].dodajPregled(new LabPregled(datum, pacijent));
        }
    }

    izaberiDoktora(pacijent, doktor) {
        const idd = SluzbaUtil.indexOfDoktor(this.doktori, doktor.ime, doktor.prezime);
        const idp = SluzbaUtil.indexOfPacijent(this.doktori, doktor.ime, doktor.prezime);

        if (idd !== -1 && idp !== -1) {
            this.pacijent.doktor = doktor;
        } else {
            console.log('pogresan unos');
        }
    }

    pronadjiPregled(datum) {
        const idx = SluzbaUtil.indexOfPregled(this.pregledi, datum);
        if (idx !== -1) {
            return this.pregledi[idx];
        }
    }

    rezultatiKP(datum, gv, dv, puls) {
        const idx = SluzbaUtil.indexOfPregled(this.pregledi, datum);
        if (idx !== -1) {
            this.pregledi[idx].gornja_vrednost = gv;
            this.predmeti[idx].donja_vrednost = dv;
            this.pregledi[idx].puls = puls;
        }
    }
    rezultatiNS(datum, v, vpo) {
        const idx = SluzbaUtil.indexOfPregled(this.pregledi, datum);
        if (idx !== -1) {
            this.pregledi[idx] = new NivoSecera(datum,v,vpo);
          
        }
    }
}

class SluzbaUtil {
    constructor() { }

    static indexOfDoktor(doktori, ime, prezime) {
        return doktori.findIndex(d => d.ime === ime && d.prezime === prezime);
    }

    static indexOfPacijent(pacijenti, ime, prezime) {
        return pacijenti.findIndex(d => d.ime === ime && d.prezime === prezime);
    }

    static indexOfPregled(pregledi, datum) {
        return pregledi.findIndex(p => p.datum === datum)
    }
}

export default ZdravstvenaUstanova;