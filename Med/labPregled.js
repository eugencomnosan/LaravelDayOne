import Pacijent from './pacijent.js';


class LabPregled {
    constructor(datum, pacijent) {
        this.datum = datum; //datum i vreme kada je zakazan
        this.pacijent = pacijent;
    }
}
// ○ krvni pritisak (gornja vrednost, donja vrednost, puls) 
// ○ nivo šećera u krvi (vrednost, vreme poslednjeg obroka) 
// ○ nivo holesterola u krvi (vrednost, vreme poslednjeg obroka) 

class KrvniPritisak extends LabPregled {
    constructor(datum, pacijent, gornja_vrednost, donja_vrednost, puls) {
        super(datum, pacijent);
        this.gornja_vrednost = gornja_vrednost || [];
        this.donja_vrednost = donja_vrednost || [];
        this.puls = puls || [];
    }

    toString() {
        return ` Pregled krvnog pritiska zakazan : ${this.datum} za pacijenta ${this.pacijent}
             Pritisak : ${this.donja_vrednost} sa: ${this.gornja_vrednost}`;
    }
}


class NivoSecera extends LabPregled {
    constructor(datum, pacijent, vrednost, vreme_poslednjeg_obroka) {
        super(datum, pacijent);
        this.vrednost = vrednost || [];
        this.vreme_poslednjeg_obroka = vreme_poslednjeg_obroka || [];
    }
    toString() {
        return `  Pregled krvnog pritiska zakazan : ${this.datum} za pacijenta ${this.pacijent}
                 nivo secera : ${this.vrednost} vreme posledjeg obroka: ${this.vreme_poslednjeg_obroka}`;
    }
}

class NivoHolesterola extends LabPregled {
    constructor(datum, pacijent, vrednost, vreme_poslednjeg_obroka) {
        super(datum, pacijent);
        this.vrednost = vrednost || [];
        this.vreme_poslednjeg_obroka = vreme_poslednjeg_obroka || [];
    }
    toString() {
        return `  Pregled krvnog pritiska zakazan : ${this.datum} za pacijenta ${this.pacijent} 
                  nivo secera : ${this.vrednost} vreme posledjeg obroka: ${this.vreme_poslednjeg_obroka}`;
    }

}

export {
    LabPregled,
    KrvniPritisak,
    NivoSecera,
    NivoHolesterola
};