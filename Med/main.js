import Doktor from './doktor.js';
import Pacijent from './pacijent.js';
import {
    LabPregled,
    KrvniPritisak,
    NivoSecera,
    NivoHolesterola

            } from './labPregled.js';
import zdravstvenaUstanova from './zdravstvenaUstanova.js';
import pacijent from './pacijent.js';


let ustanova = new zdravstvenaUstanova();

let d1 = new Doktor('Milan', 'Milanovic');
ustanova.dodajDoktora(d1);


let p1 = new Pacijent('Dragan',"Draganic",1234);
ustanova.dodajPacijenta(p1);

ustanova.izaberiDoktora(p1,d1); 

ustanova.dodajPregled(d1,p1,new NivoSecera("15 jan",p1));
ustanova.dodajPregled(d1,p1,new KrvniPritisak("16 jan",p1));

ustanova.rezultatiKP("15 jan",100,60,75);
ustanova.rezultatiNS("16 jan",9,"15 januar 5h");














// 1. kreirati doktora “Milan” 
// 2. kreirati pacijenta “Dragan” 
// 3. pacijent “Dragan” bira doktora “Milan” za svog izabranog lekara 
// 4. doktor “Milan” zakazuje pregled za merenje nivoa šećera u krvi za pacijenta “Dragan” 5. doktor “Milan” zakazuje pregled za merenje krvnog pritiska za pacijenta “Dragan” 
// 6. pacijent “Dragan” obavlja laboratorijski pregled za merenje nivoa šećera u krvi. Simulirati 
// i prikazati rezultate. 
// 7. pacijent “Dragan” obavlja laboratorijski pregled za merenje krvnog pritiska. Simulirati i 
// prikazati rezultate. 