import Doktor from './doktor.js';

class Pacijent {
	constructor(ime,prezime, jmbg, brZdravstenogKartona){
		this.ime = ime;
        this.prezime = prezime;
		this.jmbg = jmbg;
		this.brZdravstenogKartona = brZdravstenogKartona;
		this.doktor = doktor || [];
	}
	toString(){
		return `ime: ${this.ime} prezime: ${this.prezime} broj kartona : ${this.brZdravstenogKartona}`;
	}
	izaberiDoktora(doktor) {
        this.doktor =doktor;
    }
}

export default Pacijent;

