import Pacijent from './pacijent.js';
import LabPregled from './labPregled.js';

class Doktor {
	constructor(ime,prezime,specijalnost,pacijenti,zakazaniPregledi){
		this.ime = ime;
        this.prezime = prezime;
		this.specijalnost = specijalnost;
		this.pacijenti = pacijenti || [];
		this.zakazaniPregledi = zakazaniPregledi || []
	}
	toString(){
		return `ime: ${this.ime} prezime: ${this.prezime} specijalnost: ${this.specijalnost}`;
	}
}

export default Doktor;